<div id="footer">

  <p>
    <a href="copyright.htm">Copyright©&nbsp;2001-2020
    Thomas&nbsp;M.&nbsp;Eastep</a>
  </p>
  <p>
    Please report errors on this site to
    <a href="mailto:webmaster@shorewall.org?subject=Website%20Comments">the&nbsp;Webmaster</a>
  </p>
  <p>Site design by <a href="http://www.connexer.com">Connexer Ltd.</a></p>
</div>
