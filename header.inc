<div id="header">
  <div id="search-form">
    <form method="get"
          action="https://www.google.com/search">
          <p style="padding: 0; margin: 0;"> <input
          name="ie" value="UTF-8" type="hidden" /> <input
          name="oe" value="UTF-8" type="hidden" /> <input
          name="as_sitesearch" value="shorewall.org"
          type="hidden" />
    <input id="search-text" name="q" size="25" maxlength="255" value="" type="text" />
    <input id="submit-button" name="btnG" value="Site Search" type="submit" />
    </p>
    </form>
  </div>
  <div id="ml-search">
      <h4><a href="http://dir.gmane.org/gmane.comp.security.shorewall">Mailing List Archive Search</a></h4>
  </div>
  <div id="logo">
  <p>
    <img src="/images/gareth-davies-logo3.png" style="height:135px;width:622px;" alt="(Shorewall Logo)" />
    <!--
      Logo colors:
      8b8b8b - "iptables made easy"
      00bbe7 - "shore" & arrow
      0060b5 - "wall" & arrow border
    -->
  </p>
  </div>
</div>

